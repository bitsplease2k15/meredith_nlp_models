#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import re
pd.options.display.max_columns = 100
pd.options.display.max_rows = 100000
from kmodes.kmodes import KModes

import gensim
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import STOPWORDS
from nltk.stem import WordNetLemmatizer, SnowballStemmer
from nltk.stem.porter import *
from gensim import corpora, models

def wrangle_taxonomy(val):
    if r'/' not in val:
        return val + r"/"
    else:
        return val


def wrangle_tokens(val, max_val):
    if len(val) < max_val and len(val)>0:
        to_add = max_val - len(val)
        to_add = [val[-1]] * to_add
        val.extend(to_add)
        return val
    elif len(val) == max_val:
        return val
    else:
        return val

def get_cat_from_taxonomy(val):
    tax_tokens = val.split(r"/")
    if len(tax_tokens)==2:
        return tax_tokens[0]
    else:
        return tax_tokens[-1]

def clean_data_clustering(df):
    df.rename(columns={"MTAX_CATEGORY_PATH": "taxonomy", "PAGE_PATH_SLASH":"url"}, inplace=True)
    df.dropna(subset=["taxonomy"], inplace=True)
    df['taxonomy'] = df['taxonomy'].apply(lambda val: wrangle_taxonomy(val))
    df['category'] = df['taxonomy'].apply(lambda val: get_cat_from_taxonomy(val))
    df['path_tokens'] = df['taxonomy'].apply(lambda val: val.split(r'/')[:-1])
    df['path_len'] = df['path_tokens'].apply(lambda val: len(val))
    df['path_tokens'] = df['path_tokens'].apply(lambda val: wrangle_tokens(val, df['path_len'].max()))
    df['path_len'] = df['path_tokens'].apply(lambda val: len(val))

    for i in range(0, df['path_len'].max()):
        col_name = 'cat-level-'+str(i+1)
        df[col_name] = df['path_tokens'].apply(lambda val: val[i])
        df[col_name] = df[col_name].apply(lambda val: val.replace(r'/', ""))

    df.drop(['path_tokens', 'path_len'], axis=1, inplace=True)
    df['URL'] = df['url'].apply(lambda val: re.sub(r'[0-9]', '', val))
    df['URL'] = df['URL'].apply(lambda val: val.replace('/', ' '))
    df['URL'] = df['URL'].apply(lambda val: val.replace('-', ' '))
    df['URL'] = df['URL'].apply(lambda val: val.strip())
    df['url_tokens'] = df['URL'].apply(lambda val: val.split(' '))
    df['url_tokens'] = df['URL'].apply(lambda val: [token for token in val if token not in STOPWORDS])
    df['url_len'] = df['url_tokens'].apply(lambda val: len(val))

    df['url_tokens'] = df['url_tokens'].apply(lambda val: wrangle_tokens(val, df['url_len'].max()))
    df['url_len'] = df['url_tokens'].apply(lambda val: len(val))

    for i in range(0, df['url_len'].max()):
        col_name = 'url-token-'+str(i+1)
        df[col_name] = df['url_tokens'].apply(lambda val: val[i])

    df.drop(['URL', 'url_tokens', 'url_len'], axis=1, inplace=True)

    for col in df.columns[2:]:
        df[col] = df[col].apply(lambda val: val.lower())
    return df

def kmodes_clustering(df):
    num_clusters = len(df)//100
    data = df.drop(['url', 'taxonomy'], axis=1).copy()
    data.set_index("category", inplace=True)
    #REDUCE n_init variable, to reduce model runtime. TRADEOFF: cluster-quality will not be the best, & vice-versa INCREASE it to improve cluster quality
    kmode = KModes(n_clusters=num_clusters, init = "random", n_init = 3, verbose=1)
    clusters = kmode.fit_predict(data)
    data.insert(0, "cluster_num", clusters, True)
    return data

def topic_model(df):
    cat_set = set()
    
    def wrangle_cat_list(x):
        res = []
        for val in x:
            res.append(val.replace(" ", "_"))
            if val not in cat_set:
                cat_set.add(val)
        return res
        
    
    df['category'] = df['category'].apply(lambda val: wrangle_cat_list(val))
    processed_docs = df['category'].values
    dictionary = gensim.corpora.Dictionary(processed_docs)
    #INCREASE n_topics to increase no. of clusters & vice-versa
    n_topics = (len(cat_set)//10)*4
    bow_corpus = [dictionary.doc2bow(doc) for doc in processed_docs]
    lda_model = gensim.models.LdaMulticore(bow_corpus, num_topics=n_topics, id2word=dictionary, passes=2, workers=2)
    #INCREASE num_words arguement to increase no. of clusters & vice-versa
    topic_words = lda_model.show_topics(num_topics=n_topics, num_words=20, formatted=False)
    
    results = []

    for topic, word_list in topic_words:
        res_word_list = []
        for word, score in word_list:
            clean_cat = word.replace("_", " ")
            res_word_list.append((clean_cat, score))
        results.append(res_word_list)
    
    results = [sorted(val, key = lambda x: x[1], reverse=True) for val in results if len(val)>1]
    
    return results

def get_cat_clusters(df):
    df.reset_index(level=0, inplace=True)
    cat_cluster=df[['category', 'cluster_num']].drop_duplicates()
    cat_cluster = cat_cluster.groupby('cluster_num', as_index=False).agg(set)
    cat_cluster['category'] = cat_cluster['category'].apply(lambda val: sorted(list(set(val))))
    return cat_cluster

def save_results(res_arr):
    results = [(i,val) for i, val in enumerate(res_arr)]
    res_df = pd.DataFrame(results, columns=['topic', 'token_list'])
    res_df.to_csv("ensemble_model_results.csv", index=False, line_terminator="\n", sep="\t", header=True)
    
    unpiv = res_df.explode('token_list', ignore_index=True).copy()
    unpiv['category'] = unpiv['token_list'].apply(lambda val: val[0])
    unpiv['score'] = unpiv['token_list'].apply(lambda val: val[1])
    unpiv.drop(columns=['token_list'], axis=1, inplace=True)
    unpiv.to_csv("ensemble_model_results_unpiv.csv", index=False, line_terminator="\n", sep="\t", header=True)
    return unpiv

raw_data = pd.read_csv("../nlp_raw_data.csv", sep="\t")#edit this line ONLY: as per new data
clean_df = clean_data_clustering(raw_data)
cluster_df = kmodes_clustering(clean_df)
cat_clusters = get_cat_clusters(cluster_df)
cat_topics = topic_model(cat_clusters)
ensemble_res = save_results(cat_topics)

