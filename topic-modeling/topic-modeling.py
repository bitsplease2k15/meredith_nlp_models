#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import gensim
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import STOPWORDS
from nltk.stem import WordNetLemmatizer, SnowballStemmer
from nltk.stem.porter import *
import numpy as np
from gensim import corpora, models
pd.options.display.max_columns = 100
pd.options.display.max_rows = 100000
import re

def wrangle_tokens(val, max_val):
    if len(val) < max_val and len(val)>0:
        to_add = max_val - len(val)
        to_add = [val[-1]] * to_add
        val.extend(to_add)
        return val
    elif len(val) == max_val:
        return val
    else:
        return val

def wrangle_taxonomy(val):
    if r'/' not in val:
        return val+r'/'
    else:
        return val

def wrangle_taxonomy_parts(val):
    tax_list = []
    for tax_term in val:
        if tax_term != "":
            tax_term = tax_term.lower()
            tax_term = tax_term.replace(" ", "_")
            tax_list.append(tax_term)
    return tax_list

def wrangle_tax_url(val):
    url = val[0]
    tax = val[1]
    res = url.copy()
    res.extend(tax)
    return res

def wrangle_category(val):
    res = val.lower()
    res = res.replace(" ", "_")
    return res

def get_cat_from_taxonomy(val):
    tax_tokens = val.split(r"/")
    if len(tax_tokens)==2:
        return tax_tokens[0]
    else:
        return tax_tokens[-1]

def clean_data_topic_modeling(df):
    df.rename(columns={"MTAX_CATEGORY_PATH": "taxonomy", "PAGE_PATH_SLASH":"url"}, inplace=True)
    df.dropna(subset=["taxonomy"],inplace=True)
    df['taxonomy'] = df['taxonomy'].apply(lambda val: wrangle_taxonomy(val))
    df['category'] = df['taxonomy'].apply(lambda val: get_cat_from_taxonomy(val))
    df['clean_taxonomy'] = df['taxonomy'].apply(lambda val: val.split(r'/'))
    df['clean_url'] = df['url'].apply(lambda val: re.sub(r'[0-9]', '', val))
    df['clean_url'] = df['clean_url'].apply(lambda val: val.replace('/', ' '))
    df['clean_url'] = df['clean_url'].apply(lambda val: val.replace('-', ' '))
    df['clean_url'] = df['clean_url'].apply(lambda val: val.strip())
    df['clean_url'] = df['clean_url'].apply(lambda val: val.split(' '))
    df['clean_taxonomy'] = df['clean_taxonomy'].apply(lambda val: wrangle_taxonomy_parts(val))
    df['clean_url'] = df['clean_url'].apply(lambda val: [token for token in val if token not in gensim.parsing.preprocessing.STOPWORDS])
    df['url_taxonomy_doc'] = list(zip(df.clean_url, df.clean_taxonomy))
    df['url_taxonomy_doc'] = df['url_taxonomy_doc'].apply(lambda val: wrangle_tax_url(val))
    df['category'] = df['category'].apply(lambda val: wrangle_category(val))
    return df

def topic_model(df, cat_set):
    processed_docs = df['url_taxonomy_doc'].values
    dictionary = gensim.corpora.Dictionary(processed_docs)
    n_topics = len(cat_set)//10
    bow_corpus = [dictionary.doc2bow(doc) for doc in processed_docs]
    lda_model = gensim.models.LdaMulticore(bow_corpus, num_topics=n_topics, id2word=dictionary, passes=2, workers=2)
    topic_words = lda_model.show_topics(num_topics=n_topics, num_words=150, formatted=False)
    
    results = []

    for topic, word_list in topic_words:
        res_word_list = []
        for word, score in word_list:
            if word in cat_set:
                clean_cat = word.replace("_", " ")
                res_word_list.append((clean_cat, score))
        results.append(res_word_list)
    
    results = [sorted(val, key = lambda x: x[1], reverse=True) for val in results if len(val)>1]
    
    return results

def save_results(res_arr):
    results = [(i,val) for i, val in enumerate(res_arr)]
    res_df = pd.DataFrame(results, columns=['topic', 'token_list'])
    res_df.to_csv("topic_model_results.csv", index=False, line_terminator="\n", sep="\t", header=True)
    
    unpiv = res_df.explode('token_list', ignore_index=True).copy()
    unpiv['category'] = unpiv['token_list'].apply(lambda val: val[0])
    unpiv['score'] = unpiv['token_list'].apply(lambda val: val[1])
    unpiv.drop(columns=['token_list'], axis=1, inplace=True)
    unpiv.to_csv("topic_model_results_unpiv.csv", index=False, line_terminator="\n", sep="\t", header=True)
    
    return unpiv

raw_data = pd.read_csv("../nlp_raw_data.csv", sep="\t")#edit this line--to account for new data--ensure schema is the same
clean_data = clean_data_topic_modeling(raw_data)
cat_set = set(clean_data['category'].unique())
model_results = topic_model(clean_data, cat_set)
res_df = save_results(model_results)




