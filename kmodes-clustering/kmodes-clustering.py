#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import numpy as np
import re
from gensim.parsing.preprocessing import STOPWORDS
pd.options.display.max_columns = 100
pd.options.display.max_rows = 100000
from kmodes.kmodes import KModes

def wrangle_taxonomy(val):
    if r'/' not in val:
        return val + r"/"
    else:
        return val

def wrangle_tokens(val, max_val):
    if len(val) < max_val and len(val)>0:
        to_add = max_val - len(val)
        to_add = [val[-1]] * to_add
        val.extend(to_add)
        return val
    elif len(val) == max_val:
        return val
    else:
        return val

def get_cat_from_taxonomy(val):
    tax_tokens = val.split(r"/")
    if len(tax_tokens)==2:
        return tax_tokens[0]
    else:
        return tax_tokens[-1]

def clean_data_clustering(df):
    df.rename(columns={"MTAX_CATEGORY_PATH": "taxonomy", "PAGE_PATH_SLASH":"url"}, inplace=True)
    df.dropna(subset=["taxonomy"], inplace=True)
    df['taxonomy'] = df['taxonomy'].apply(lambda val: wrangle_taxonomy(val))
    df['category'] = df['taxonomy'].apply(lambda val: get_cat_from_taxonomy(val))
    df['path_tokens'] = df['taxonomy'].apply(lambda val: val.split(r'/')[:-1])
    df['path_len'] = df['path_tokens'].apply(lambda val: len(val))
    df['path_tokens'] = df['path_tokens'].apply(lambda val: wrangle_tokens(val, df['path_len'].max()))
    df['path_len'] = df['path_tokens'].apply(lambda val: len(val))

    for i in range(0, df['path_len'].max()):
        col_name = 'cat-level-'+str(i+1)
        df[col_name] = df['path_tokens'].apply(lambda val: val[i])
        df[col_name] = df[col_name].apply(lambda val: val.replace(r'/', ""))

    df.drop(['path_tokens', 'path_len'], axis=1, inplace=True)
    df['URL'] = df['url'].apply(lambda val: re.sub(r'[0-9]', '', val))
    df['URL'] = df['URL'].apply(lambda val: val.replace('/', ' '))
    df['URL'] = df['URL'].apply(lambda val: val.replace('-', ' '))
    df['URL'] = df['URL'].apply(lambda val: val.strip())
    df['url_tokens'] = df['URL'].apply(lambda val: val.split(' '))
    df['url_tokens'] = df['URL'].apply(lambda val: [token for token in val if token not in STOPWORDS])
    
    df['url_len'] = df['url_tokens'].apply(lambda val: len(val))

    df['url_tokens'] = df['url_tokens'].apply(lambda val: wrangle_tokens(val, df['url_len'].max()))
    df['url_len'] = df['url_tokens'].apply(lambda val: len(val))

    for i in range(0, df['url_len'].max()):
        col_name = 'url-token-'+str(i+1)
        df[col_name] = df['url_tokens'].apply(lambda val: val[i])

    df.drop(['URL', 'url_tokens', 'url_len'], axis=1, inplace=True)

    for col in df.columns[2:]:
        df[col] = df[col].apply(lambda val: val.lower())
    return df

def kmodes_clustering(df):
    num_clusters = len(df)//100 #INCREASE to get more clusters & vice-versa
    data = df.drop(['url', 'taxonomy'], axis=1).copy()
    data.set_index("category", inplace=True)
    kmode = KModes(n_clusters=num_clusters, init = "random", n_init = 3, verbose=1) #decrease n_init for lower-model runtime, will reduce cluster quality too & vice-versa (increase ==> better clusters)
    clusters = kmode.fit_predict(data)
    data.insert(0, "cluster_num", clusters, True)
    return data

def save_results(df):
    df.reset_index(level=0, inplace=True)
    cat_cluster=df[['category', 'cluster_num']].drop_duplicates()
    cat_cluster = cat_cluster.groupby('cluster_num', as_index=False).agg(set)
    cat_cluster['category'] = cat_cluster['category'].apply(lambda val: sorted(list(set(val))))
    cat_cluster.to_csv("categorical_clustering_results.csv", index=False, 
                       header=True, line_terminator="\n", sep="\t")
    cat_cluster_unpiv = cat_cluster.explode('category', ignore_index=True).copy()
    cat_cluster_unpiv.to_csv("categorical_clustering_results_unpiv.csv", sep="\t", index=False, header=True,
                             line_terminator="\n")
    return cat_cluster_unpiv

raw_data = pd.read_csv("../nlp_raw_data.csv", sep="\t")
clean_df = clean_data_clustering(raw_data)
cluster_df = kmodes_clustering(clean_df)
cat_clusters = save_results(cluster_df)

