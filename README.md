### Setup/Installation
* Clone/download the repo/folder to a location of your choice
* Open the terminal (Mac)/command-prompt (Windows) and type the following command:
```shell
pip3 install -r requirements.txt
```
* If above command fails try:
```shell
pip install -r requirements.txt
```
* If you use anaconda--add anaconda to system PATH or open anaconda-prompt and try:
```shell
conda install -r requirements.txt
```

### Problem

* Cluster "categories", i.e. last/most granular element of a **MTAX_CATEGORY_PATH**  
* As one can see the green highlighted cells should be clustered together--i.e. bedding, bedroom, bedroom details, mattresses, bed pillows & beds  
* Sure, these can be combined under **"higher" taxonomy terms using the mtax_path**--i.e. all can be grouped under **TOPCATEGORY Home**--bedroom details, mattresses & pillows can be grouped under **home interior features**  
* But besides this we also want to capture categories that have no similarity on the mtax_path whatsover, but still make **"sense" from a narrative POV**--i.e. adult sleep concerns & child sleep concerns, have no similarity on the mtax_path with each other or the bed related categories, but it still makes sense to view all these together  
* **Conclusion--build a model that creates a cluster of the categories defined in green, yellow & red cells**. Another cluster could be (sleep concerns, exercise, diet), (christmas, jewelry, gifts, parties) etc--so on & so forth  
  
![problem](./readme_imgs/model-1.png)

### Solution-- Unsupervised Models
* kmodes clustering
* topic modeling
* ensemble model: run kmodes clustering, and use that output for training the the topic model

### Raw Data (when using your own datasets-- ensure exact schema i.e. column names)
* Here is a sample **snowflake** query fetch raw data:-- 
~~~~sql
select distinct PAGE_PATH_SLASH, MTAX_CATEGORY_PATH
from "DEV"."SANDBOX"."NV_CG_MTAX_AGGREGATE"
where brand in ('parents', 'shp', 'travelandleisure');
~~~~
* Nate/DAI might ask you to only fetch certain brands/topcategories etc--so filter rows as necessary

![rawdata](./readme_imgs/raw-data.png)

### KModes-Clustering
```python
raw_data = pd.read_csv("../nlp_raw_data.csv", sep="\t") #change this line ONLY, point to your new data file, change read method to excel if you want or change seperators
clean_df = clean_data_clustering(raw_data)
cluster_df = kmodes_clustering(clean_df)
cat_clusters = save_results(cluster_df)# saves the results in a tab seperated csv file called categorical_clustering_results.csv under kmodes-clustering/ (i.e. directory)
```
  
* The code is availabe as both a jupyter notebook or python script
* kmodes-clustering/kmodes-clustering.ipynb: edit cell 8 only--add your file path for the new data
* kmodes-clustering/kmodes-clustering.py: edit line 90 only as per new data

### Topic-Modeling
```python
raw_data = pd.read_csv("../nlp_raw_data.csv", sep="\t")#edit this line ONLY--to account for new data--ensure schema is the same
clean_data = clean_data_topic_modeling(raw_data)
cat_set = set(clean_data['category'].unique())
model_results = topic_model(clean_data, cat_set)
res_df = save_results(model_results)# save the results in tab seperated csv file called topic_model_results.csv under topic-modeling/ (i.e. directory)
```

* The code is availabe as both a jupyter notebook or python script
* topic-modeling/topic-modeling.ipynb: edit cell 11 only--add your file path for the new data
* topic-modeling/topic-modeling.py: edit line 106 only--add your file path for the new data

### Ensemble-Model (kmodes+topic modeling)
```python
raw_data = pd.read_csv("../nlp_raw_data.csv", sep="\t")#edit this line ONLY: as per new data
clean_df = clean_data_clustering(raw_data)
cluster_df = kmodes_clustering(clean_df)
cat_clusters = get_cat_clusters(cluster_df)
cat_topics = topic_model(cat_clusters)
ensemble_res = save_results(cat_topics)#save the result in a tab seperated csv file called ensemble_model_results.csv under ensemble-model/ (i.e. directory)
```

* The code is availabe as both a jupyter notebook or python script
* ensemble-model/ensemble-model.ipynb: edit cell 3 only--add your file path/change read method for new data
* ensemble-model/ensemble-model.py: edit line 133 only--add your file path/change read method for new data

### Model Tradeoffs

| PAGE_PATH_SLASH | MTAX_CATEGORY_PATH | clean_url_taxonomy_tokens |
| --------------- | ------------------ | ------------------------- |
| /9660/diy/indoor-plant-growth-in-nyc/| Garden/plants/plant duration| ("diy", "indoor", "plant", "growth", "nyc", "garden", "plants", **"plant duration"**)|
| /8125116/hydrangea-growth-and-care-guide/| Garden/plants/plant type| ("hydrangea", "growth", "care", "guide", "garden", "plants", **"plant type"**)| 

* **KModes-Clustering** ==> calculates similarity-score b/w these **2 rows/records** = 3, since we've **3 matching tokens aka growth, garden & plants**, and based on these similarity scores clusters points/rows together. **Note each row/record is considered an individual point**--thus model is more computationally expensive to run BUT produces **exhaustive** & **complete** results. Read here for more:-- [kmodes-explained](https://www.analyticsvidhya.com/blog/2021/06/kmodes-clustering-algorithm-for-categorical-data/)
* **Topic-Modelng** ==> **reduces the search space down** FIRST (as shown below) ==> then clusters words into topics based on **frequencies** ==> note since each row isn't considered a single point--this model is far more faster to run--but does not produce complete results. Read here for more:-- [LDA-topic-model](https://towardsdatascience.com/end-to-end-topic-modeling-in-python-latent-dirichlet-allocation-lda-35ce4ed6b3e0)
     1. "diy":1, "indoor":1, "plant":1, "care":1, "hydrangea":1, "guide":1, "nyc":1, "plant duration":1, "plant type":1, "growth":2, "garden":2, "plants":2

* **Conclusion** ==> run kmodes on smaller datasets & topic models for larger ones (kmodes takes 3.5 hrs currently on 16GB RAM, 4-core i-7 Intel for 170k records, compared to topic-model's 5 min)

### Model Parameter Controls (no. of clusters & categories/items per cluster)
* **KModes-Clustering:** only allows one to ONLY control the number of clusters. change the following line of code in the jupyter-notebook/python script to change the number of clusters (**cell-6** in the jupyter-notebook OR **line-74** in python script)
```python
def kmodes_clustering(df):
    num_clusters = len(df)//100 #CHANGE THIS LINE TO ADJUST NO. OF CLUSTERS
    data = df.drop(['url', 'taxonomy'], axis=1).copy()
    data.set_index("category", inplace=True)
    kmode = KModes(n_clusters=num_clusters, init = "random", n_init = 3, verbose=1)
    clusters = kmode.fit_predict(data)
    data.insert(0, "cluster_num", clusters, True)
    return data
```
* **Topic-Model:** allows one control BOTH no. of clusters/topics and items/words per cluster. But was producing poor results so it was combined with KModes to produce a ensemble model

### Ensemble (KModes + Topic modeling):-- optimal model w/ complete controls
* The input dataset is run through KModes-clustering first--and the output is used to train a topic model
* To control the **no. of clusters** & **items per cluster**, change the values of the following variables (**cell-2 under method topic_model** in jupyter-notebook OR **line-104 & line-107** in the script):
```python    
df['category'] = df['category'].apply(lambda val: wrangle_cat_list(val))
processed_docs = df['category'].values
dictionary = gensim.corpora.Dictionary(processed_docs)
n_topics = (len(cat_set)//10)*4 #ADJUST this value to control no. of clusters
bow_corpus = [dictionary.doc2bow(doc) for doc in processed_docs]
lda_model = gensim.models.LdaMulticore(bow_corpus, num_topics=n_topics, id2word=dictionary, passes=2, workers=2)
#CHANGE the value of num_words to control no. of items per topic/cluster--hardcoded to 20 at the moment
topic_words = lda_model.show_topics(num_topics=n_topics, num_words=20, formatted=False)
```
* **Tradeoffs**: since the ensemble/combined model runs Kmodes first-- it is computationally expensive, much better to use on smaller datasets (3.5 hrs + 5 min for 170k records on 16GB RAM, 4-core i-7 Intel)